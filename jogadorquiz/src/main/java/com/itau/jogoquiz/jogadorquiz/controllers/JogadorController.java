package com.itau.jogoquiz.jogadorquiz.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itau.jogoquiz.jogadorquiz.models.Jogador;
import com.itau.jogoquiz.jogadorquiz.repositories.JogadorRepository;
import com.itau.jogoquiz.jogadorquiz.services.JWTService;
import com.itau.jogoquiz.jogadorquiz.services.PasswordService;

@RestController
@RequestMapping("/jogador")
@CrossOrigin
public class JogadorController {
	@Autowired
	JogadorRepository jogadorRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	JWTService jwtService;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscarJogadores(@RequestParam(value = "nome", required = false) String nome){
		if(nome == null)
			return jogadorRepository.findAll();
		else
			return jogadorRepository.findByNome(nome).get();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/{id}")
	public ResponseEntity<?> buscarJogadores(@PathVariable long id) {
		Optional<Jogador> optional = jogadorRepository.findById(id);
		if(!optional.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(optional.get());
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Jogador criarJogador(@RequestBody Jogador jogador) {
		String hash = passwordService.gerarHash(jogador.getSenha());
		jogador.setSenha(hash);
		return jogadorRepository.save(jogador);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/login")
	public ResponseEntity<?> validarJogador(@RequestBody Jogador jogador) {
		Optional<List<Jogador>> jog = jogadorRepository.findByNome(jogador.getNome());
		if(!jog.isPresent())
			return ResponseEntity.badRequest().build();
		
		Jogador jogadorGet = jog.get().get(0);
		if(passwordService.verificarHash(jogador.getSenha(), jogadorGet.getSenha())) {
			String token = jwtService.gerarToken(jogadorGet.getNome());
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			return new ResponseEntity<Jogador>(jogadorGet, headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}
}
