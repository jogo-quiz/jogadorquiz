package com.itau.jogoquiz.jogadorquiz.services;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

@Service
public class JWTService {
	String key = "jogoquiz";
	Algorithm algorithm = Algorithm.HMAC256(key);
	JWTVerifier verifier = JWT.require(algorithm).build();
	
	public String gerarToken(String nome) {
		return JWT.create().withClaim("nome", nome).sign(algorithm);
	}
	
	public String validarToken(String token) {
		try {
			return verifier.verify(token).getClaim("nome").asString();
		}catch (Exception e) {
			return null;
		}
	}
}
