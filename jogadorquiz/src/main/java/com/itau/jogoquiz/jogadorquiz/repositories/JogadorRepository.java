package com.itau.jogoquiz.jogadorquiz.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.jogoquiz.jogadorquiz.models.Jogador;

public interface JogadorRepository  extends CrudRepository<Jogador, Long>{
	
	public Optional<List<Jogador>> findByNome(String nome);
	
}
